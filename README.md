<picture>
  <source media="(prefers-color-scheme: dark)" srcset="./assets/beega-banner.png">
  <source media="(prefers-color-scheme: light)" srcset="./assets/beega-banner.png">
  <img alt="Alternate logo" src="./assets/beega-banner.png">
</picture>

### 👋 Introduction

We are an identity and access management platform.

### 🎯 Objective

It is an identity and access management platform for India, ensuring compliance with local data protection laws while offering robust, multi-layered authentication methods.

 It will consist of the following features:
    
  - Support for Aadhaar-based authentication and multi-factor authentication (MFA) to accommodate the varying security needs in India.
  - Adherence to laws such as the IT Act, DPDP Act, and other local regulatory requirements to safeguard user data and privacy.


### 🌈 Contribution guidelines

We appreciate any and all contributions, and hence we have outlined contribution guidelines for each repo or project.

It is done so because some require a basic signing requirement for contributions (we call it the **Level 1** signing requirement) and some require a special signing requirement for contributions (we call it the **Level 0** signing requirement).

We kindly request that you review each of them before you start working on the repo or project.


### 👩‍💻 Useful resources

#### Social:
  - Website: https://www.beega.in
  - LinkedIn: https://www.linkedin.com/company/beega-india

> Note: We are only on above mentioned social platforms, and it is updated every minute.

#### Logo:

Beega logo in Yellow |    Beega logo in Yellow
:-------------------------: |  :-------------------------:
![ Bashe logo in Yellow](./assets/beega-logo.png) | ![ Bashe logo in Yellow](./assets/beega-logo.png)
